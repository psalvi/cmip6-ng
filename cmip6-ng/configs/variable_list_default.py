#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: A basic config file containing all variables and scenarios to run.
After checking out cd into the main directory and link this file like
ln -s variable_lists/variable_list_default.py variable_list.py
To use another configuration delete the link and recreate is correspondingly
ln -s >variable_lists/variable_list_user.py< variable_list.py

It is save to import * from this file.
"""
# -- fixed variables --
FX_RUN_TYPES = [
    'land-hist',
    'piControl',
    '1pctCO2',
    'abrupt-2xCO2',
    'abrupt-4xCO2',
    'historical',
    'ssp119',
    'ssp126',
    'ssp245',
    'ssp370',
    'ssp434',
    'ssp460',
    'ssp534-over',
    'ssp585',
    '1pctCO2-bgc',
    'esm-1pctCO2',
    'esm-hist',
    'esm-ssp585',
    'hist-bgc',
    'ssp534-over-bgc',
    'ssp585-bgc',
    'amip-piForcing',
]

FX_VARNS = [
    'areacella',
    'sftlf',
]

# -- atmospheric variables --
AMON_RUN_TYPES = [
    'land-hist',
    'piControl',
    'control-1950',
    '1pctCO2',
    'abrupt-2xCO2',
    'abrupt-4xCO2',
    'historical',
    'ssp119',
    'ssp126',
    'ssp245',
    'ssp370',
    'ssp434',
    'ssp460',
    'ssp534-over',
    'ssp585',
    '1pctCO2-bgc',
    'esm-1pctCO2',
    'esm-hist',
    'esm-ssp585',
    'hist-bgc',
    'hist-nat',
    'hist-aer',
    'hist-GHG',
    'hist-1950',
    'ssp534-over-bgc',
    'ssp585-bgc',
    'amip-piForcing',
]

AMON_VARNS = [
    'clt',
    'evspsbl',
    'hurs',
    'huss',
    'hfls',
    'pr',
    'rlut',
    'rlutcs',
    'rsdt',
    'rsut',
    'rsutcs',
    'rtmt',
    'ta',
    'tas',
    'tasmax',
    'tasmin',
    'tauu',
    'tauv',
    'rsus',
    'rsuscs',
    'rsds',
    'rsdscs',
    'rlus',
    'rlds',
    'rldscs',
    'prw',
    'psl',
    'hfss',
    'hfls',
    'zg500',
    'co2mass',
    'uas',
    'vas',
]

# -- ocean variables --
OMON_RUN_TYPES = [
    'piControl',
    'historical',
    'ssp119',
    'ssp126',
    'ssp245',
    'ssp370',
    'ssp434',
    'ssp460',
    'ssp534-over',
    'ssp585',
    'amip-piForcing',
]

OMON_VARNS = [
    'tos',
    'hfds',
    'zos',
]

# -- sea ice variables --
SIMON_RUN_TYPES = [
    'historical',
    'ssp119',
    'ssp126',
    'ssp245',
    'ssp370',
    'ssp434',
    'ssp460',
    'ssp534-over',
    'ssp585',
]


SIMON_VARNS = [
    'siconc',
]

# -- land variables --
LMON_RUN_TYPES = [
    'piControl',
    'historical',
    'ssp119',
    'ssp126',
    'ssp245',
    'ssp370',
    'ssp434',
    'ssp460',
    'ssp534-over',
    'ssp585',
    '1pctCO2-bgc',
    'esm-1pctCO2',
    'esm-hist',
    'esm-ssp585',
    'hist-bgc',
    'hist-nat',
    'land-hist',
    'ssp534-over-bgc',
    'ssp585-bgc',
]

LMON_VARNS = [
    'tran',
    'evspsblveg',
    'evspsblsoi',
    'gpp',
    'npp',
    'ra',
    'rh',
    'nbp',
    'mrso',
    'mrsos',
    'mrro',
    'mrros',
    'tran',
    'lai',
    'tsl',
    'treeFrac',
]

EMON_RUN_TYPES = [
    'piControl',
    'historical',
    'ssp119',
    'ssp126',
    'ssp245',
    'ssp370',
    'ssp434',
    'ssp460',
    'ssp534-over',
    'ssp585',
    '1pctCO2-bgc',
    'esm-1pctCO2',
    'esm-hist',
    'esm-ssp585',
    'hist-bgc',
    'hist-nat',
    'land-hist',
    'ssp534-over-bgc',
    'ssp585-bgc',
]

EMON_VARNS = [
    'mrsol',
]

# daily variables
DAY_RUN_TYPES = [
    'piControl',
    'historical',
    'ssp119',
    'ssp126',
    'ssp245',
    'ssp370',
    'ssp434',
    'ssp460',
    'ssp534-over',
    'ssp585',
    'esm-hist',
    'esm-ssp585',
    'esm-1pctCO2',
]

DAY_VARNS = [
    'tas',
    'tasmin',
    'tasmax',
    'pr',
    'mrro',
]

AERDAY_RUN_TYPES = [
    'piControl',
    'historical',
    'ssp119',
    'ssp126',
    'ssp245',
    'ssp370',
    'ssp434',
    'ssp460',
    'ssp534-over',
    'ssp585',
]

AERDAY_VARNS = [
    'zg500',
]
