#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: A basic config file containing all variables and scenarios to run.
After checking out cd into the main directory and link this file like
ln -s variable_lists/variable_list_default.py variable_list.py
To use another configuration delete the link and recreate is correspondingly
ln -s >variable_lists/variable_list_user.py< variable_list.py

It is save to import * from this file.
"""
# -- fixed variables --
FX_RUN_TYPES = [
    'land-hist',
    'piControl',
    '1pctCO2',
    'abrupt-2xCO2',
    'abrupt-4xCO2',
    'historical',
    'ssp119',
    'ssp126',
    'ssp245',
    'ssp370',
    'ssp434',
    'ssp460',
    'ssp534-over',
    'ssp585',
    '1pctCO2-bgc',
    'esm-1pctCO2',
    'esm-hist',
    'esm-ssp585',
    'hist-bgc',
    'ssp534-over-bgc',
    'ssp585-bgc',
]

FX_VARNS = [
    'areacella',
    'sftlf',
]

# -- atmospheric variables --
AMON_RUN_TYPES = [
    'hist-nat',
    'hist-aer',
    'hist-GHG',
]

AMON_VARNS = [
    'clt',
    'evspsbl',
    'hurs',
    'huss',
    'hfls',
    'pr',
    'rlut',
    'rlutcs',
    'rsdt',
    'rsut',
    'rsutcs',
    'rtmt',
    'ta',
    'tas',
    'tasmax',
    'tasmin',
    'tauu',
    'tauv',
    'rsus',
    'rsuscs',
    'rsds',
    'rsdscs',
    'rlus',
    'rlds',
    'rldscs',
    'prw',
    'psl',
    'hfss',
    'hfls',
    'zg500',
    'co2mass',
]

# -- atmospheric variables, at specified plev in hPa
AMON_VARNS_PLEV = [
    ('zg',500),
    ('ta',700),
]

# -- ocean variables --
OMON_RUN_TYPES = [
]

OMON_VARNS = [
]

# -- sea ice variables --
SIMON_RUN_TYPES = [
]


SIMON_VARNS = [
]

# -- land variables --
LMON_RUN_TYPES = [
    'hist-bgc',
    'hist-nat',
]

LMON_VARNS = [
    'tran',
    'evspsblveg',
    'evspsblsoi',
    'gpp',
    'npp',
    'ra',
    'rh',
    'nbp',
    'mrso',
    'mrsos',
    'mrro',
    'mrros',
    'tran',
    'lai',
    'tsl',
    'treeFrac',
]

EMON_RUN_TYPES = [
    'hist-bgc',
    'hist-nat',
]

EMON_VARNS = [
    'mrsol',
]

# daily variables
DAY_RUN_TYPES = [
]

DAY_VARNS = [
    'tas',
    'tasmin',
    'tasmax',
    'pr',
]

AERDAY_RUN_TYPES = [
]

AERDAY_VARNS = [
    'zg500',
]
