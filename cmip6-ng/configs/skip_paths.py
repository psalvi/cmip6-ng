#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: skip_path_patterns is a list of strings giving paths which will be
skipped by the CMIP6ng processor. The use of wildcards is allowed, they will be
expanded using glob.
"""
from glob import glob

skip_path_patterns = [
    # 2020-05-10, via Mathias: There is a massive drop in global mean teperature towards the end of the 21st century of about 6K to 8K
    # 2022-10-21, via Vincent: File was updated 2020-05-12 and the error could not be reproduced anymore
    #'/net/atmos/data/cmip6/ssp585/Amon/tas/CIESM/r1i1p1f1/gr/*',

    # "All NINT (p1) ssp* runs for GISS-E2-1-G and GISS-E2-1-H that were uploaded in Feb 2020 have been withdrawn because of an inadvertent discontinuity in forcings between them and the historical runs. These will be replaced as soon as possible. Note that the p3 SSP runs are fine."
    '/net/atmos/data/cmip6/ssp*/*/*/GISS-E2-1-*/*p1f*/*',  #

    '/net/atmos/data/cmip6/*/Amon/hurs/MPI-ESM1-2-HR/*/*/*',  # fraction instead of %

    '/net/atmos/data/cmip6/piControl/Amon/*/EC-Earth3-LR/r1i1p1f1/gr/',  # no latitude dimension

    '/net/atmos/data/cmip6/*/Amon/hurs/MCM-UA-1-0/*/gn/',  # completely wrong hurs

    '/net/atmos/data/cmip6/*/Amon/pr/MCM-UA-1-0/*/gn/',  # completely wrong pr

    # '/net/atmos/data/cmip6/historical/Amon/*/NorCPM1/*/gn/',  # historical until 2029 -> fixed

    '/net/atmos/data/cmip6/*/Amon/tasmin/CESM2-WACCM/*/gn/',  # identical to tas
    '/net/atmos/data/cmip6/*/Amon/tasmax/CESM2-WACCM/*/gn/',
    '/net/atmos/data/cmip6/*/Amon/tasmin/CESM2/*/gn/',
    '/net/atmos/data/cmip6/*/Amon/tasmax/CESM2/*/gn/',

    '/net/atmos/data/cmip6/abrupt-4xCO2/Amon/*/*/*/*/*_abrupt-4xCO2_????02-??????.nc',  # skip everything that does not starts in January
    '/net/atmos/data/cmip6/abrupt-4xCO2/Amon/*/*/*/*/*_abrupt-4xCO2_????03-??????.nc',
    '/net/atmos/data/cmip6/abrupt-4xCO2/Amon/*/*/*/*/*_abrupt-4xCO2_????04-??????.nc',
    '/net/atmos/data/cmip6/abrupt-4xCO2/Amon/*/*/*/*/*_abrupt-4xCO2_????05-??????.nc',
    '/net/atmos/data/cmip6/abrupt-4xCO2/Amon/*/*/*/*/*_abrupt-4xCO2_????06-??????.nc',
    '/net/atmos/data/cmip6/abrupt-4xCO2/Amon/*/*/*/*/*_abrupt-4xCO2_????07-??????.nc',
    '/net/atmos/data/cmip6/abrupt-4xCO2/Amon/*/*/*/*/*_abrupt-4xCO2_????08-??????.nc',
    '/net/atmos/data/cmip6/abrupt-4xCO2/Amon/*/*/*/*/*_abrupt-4xCO2_????09-??????.nc',
    '/net/atmos/data/cmip6/abrupt-4xCO2/Amon/*/*/*/*/*_abrupt-4xCO2_????10-??????.nc',
    '/net/atmos/data/cmip6/abrupt-4xCO2/Amon/*/*/*/*/*_abrupt-4xCO2_????11-??????.nc',
    '/net/atmos/data/cmip6/abrupt-4xCO2/Amon/*/*/*/*/*_abrupt-4xCO2_????12-??????.nc',

    # 2019-11-28 09:22:19,603 - ERROR - check_range_full() 122: hurs (%): 4.2991 < 0.0 or 3508.8774 > 2300.0
    '/net/atmos/data/cmip6/piControl/Amon/hurs/SAM0-UNICON/r1i1p1f1/gn/',

    # 2019-11-28 09:22:43,114 - ERROR - check_range_full() 122: hurs (%): 4.1541 < 0.0 or 23779.3301 > 2300.0
    '/net/atmos/data/cmip6/piControl/Amon/hurs/FIO-ESM-2-0/r1i1p1f1/gn/',

    # 2019-11-28 09:23:59,820 - ERROR - check_range_full() 122: rtmt (W m-2): 88.2126 < -800.0 or 751.2267 > 300.0
    '/net/atmos/data/cmip6/piControl/Amon/rtmt/FIO-ESM-2-0/r1i1p1f1/gn/',

    # 2019-11-28 09:25:43,315 - ERROR - check_range_full() 122: rsus (W m-2): -73.7919 < -999 or 27280.5957 > 999
    '/net/atmos/data/cmip6/piControl/Amon/rsuscs/EC-Earth3-Veg/r1i1p1f1/gr/',

    # 2019-11-28 09:26:38,782 - ERROR - check_range_full() 122: rsus (W m-2): -81.7718 < -999 or 25805.4512 > 999
    '/net/atmos/data/cmip6/piControl/Amon/rsuscs/EC-Earth3/r1i1p1f1/gr/',

    # 2019-11-28 09:28:01,306 - ERROR - check_range_full() 122: rsds (W m-2): 0.0000 < -999 or 27326.3711 > 999
    '/net/atmos/data/cmip6/piControl/Amon/rsdscs/EC-Earth3-Veg/r1i1p1f1/gr/',

    # 2019-11-28 09:29:04,792 - ERROR - check_range_full() 122: rsds (W m-2): 0.0000 < -999 or 26017.4688 > 999
    '/net/atmos/data/cmip6/piControl/Amon/rsdscs/EC-Earth3/r1i1p1f1/gr/',

    # 2019-11-28 09:33:30,002 - ERROR - check_range_full() 122: rsus (W m-2): -79.0486 < -999 or 16603.8867 > 999
    '/net/atmos/data/cmip6/1pctCO2/Amon/rsuscs/EC-Earth3-Veg/r1i1p1f1/gr/',

    # 2019-11-28 09:33:58,202 - ERROR - check_range_full() 122: rsus (W m-2): -78.2886 < -999 or 26308.6289 > 999
    '/net/atmos/data/cmip6/1pctCO2/Amon/rsuscs/EC-Earth3/r3i1p1f1/gr/',

    # 2019-11-28 09:34:33,050 - ERROR - check_range_full() 122: rsds (W m-2): 0.0000 < -999 or 16783.8223 > 999
    '/net/atmos/data/cmip6/1pctCO2/Amon/rsdscs/EC-Earth3-Veg/r1i1p1f1/gr/',

    # 2019-11-28 09:35:10,388 - ERROR - check_range_full() 122: rsds (W m-2): 0.0000 < -999 or 26548.1445 > 999
    '/net/atmos/data/cmip6/1pctCO2/Amon/rsdscs/EC-Earth3/r3i1p1f1/gr/',
]

skip_paths = []
for skip_path_pattern in skip_path_patterns:
    skip_paths += glob(skip_path_pattern.rstrip('/'))
