#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: A basic config file containing all variables and scenarios to run.
After checking out cd into the main directory and link this file like
ln -s variable_list_default.py variable_list.py
To use another configuration delete the link and recreate is correspondingly
ln -s >variable_list_user.py< variable_list.py

It is save to import * from this file.
"""
# -- fixed variables --
FX_RUN_TYPES = [
]

FX_VARNS = [
]

# -- atmospheric variables --
AMON_RUN_TYPES = [
    'historical',
    'ssp119',
    'ssp126',
    'ssp245',
    'ssp370',
    'ssp434',
    'ssp460',
    'ssp534-over',
    'ssp585',
    '1pctCO2',
    'amip-piForcing', 
]

AMON_VARNS = [
    'tas',
]

# annual variables
ANNUAL_VARNS = [
    'tas',
]

# -- ocean variables --
OMON_RUN_TYPES = [
]

OMON_VARNS = [
]

# -- sea ice variables --
SIMON_RUN_TYPES = [
]


SIMON_VARNS = [
]

# -- land variables --
LMON_RUN_TYPES = [
]

LMON_VARNS = [
]

EMON_RUN_TYPES = [
]

EMON_VARNS = [
]

# daily variables
DAY_RUN_TYPES = [
]

DAY_VARNS = [
]

AERDAY_RUN_TYPES = [
]

AERDAY_VARNS = [
]
