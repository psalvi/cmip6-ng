#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: A basic config file containing all variables and scenarios to run.
After checking out cd into the main directory and link this file like
ln -s variable_lists/variable_list_default.py variable_list.py
To use another configuration delete the link and recreate is correspondingly
ln -s >variable_lists/variable_list_user.py< variable_list.py

It is save to import * from this file.
"""
# -- atmospheric variables --
AMON_RUN_TYPES = [
    'land-hist',
]

AMON_VARNS = [
    'evspsbl',
    'hfls',
    'pr',
]

# -- ocean variables --
OMON_RUN_TYPES = [
]

OMON_VARNS = [
]

# -- sea ice variables --
SIMON_RUN_TYPES = [
]


SIMON_VARNS = [
]

# daily variables
DAILY_RUN_TYPES = [
]

DAILY_VARNS = [
]

# annual variables
ANNUAL_VARNS = [
    'evspsbl',
    'hfls',
    'pr',
]
