#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
# range for the full range (i.e., min/max of time, lat, lon)
ERROR_RANGE = {
    'areacella': (0, 1.e11),  # Grid-Cell Area for Atmospheric Grid Variables (m2)
    'clt': (0., 100.1),  # Total Cloud Cover Percentage (%)
    'co2mass': (-999999, 999999),  # Total Atmospheric Mass of CO2 (kg)
    'evspsbl': (-.0007, .007),  # Evaporation (kg m-2 s-1)
    'evspsblveg': (-.0007, .007),  # Evaporation from Canopy (kg m-2 s-1)
    'evspsblsoi': (-.0007, .007),  # Water Evaporation from Soil (kg m-2 s-1)
    'gpp': (-9999, 9999),  # Carbon Mass Flux out of Atmosphere due to Gross Primary Production on Land (kg m-2 s-1)
    'lai': (-1, 50),  # Leaf Area Index (1)
    'mrso': (-9999, 9999),  # Total Soil Moisture Content (kg m-2)
    'mrsol': (-9999, 9999),  # Total Water Content of Soil Layer (kg m-2)
    'nbp': (-9999, 9999),  # Carbon Mass Flux out of Atmosphere Due to Net Biospheric Production on Land (kg m-2 s-1)
    'npp': (-9999, 9999),  # Net Primary Production on Land as Carbon Mass Flux (kg m-2 s-1)
    'hfss': (-999, 999),  # Surface Upward Sensible Heat Flux (W m-2)
    'hfls': (-999, 999),  # Surface Upward Latent Heat Flux (W m-2)
    'hfds': (-9999, 9999),  # Downward Heat Flux at Sea Water Surface (W m-2)
    'hurs': (0., 2300.),  # Near-Surface Relative Humidity (%)
    'huss': (-0.01, 1.),  # Near-Surface Specific Humidity (kg/kg)
    'mrro': (-2, 2),  # Total Runoff (kg m-2 s-1)
    'npp': (-9999, 9999),  # Carbon Mass Flux out of Atmosphere due to Net Primary Production on Land (kg m-2 s-1)
    'pr': (-0.001, 0.03),  # Precipitation (kg m-2 s-1)
    'prw': (-999, 999),  # Water Vapor Path (vertically integrated through the atmospheric column) (kg m-2)
    'psl': (80000, 120000),  # Sea Level Pressure (Pa)
    'ra': (-9999, 9999),  # Carbon Mass Flux into Atmosphere Due to Autotrophic (Plant) Respiration on Land (kg m-2 s-1)
    'rh': (-9999, 9999),  # Carbon Mass Flux into Atmosphere Due to Heterotrophic Respiration on Land (kg m-2 s-1)
    'rlds': (-100, 2000),  # Surface Downwelling Longwave Radiation (W m-2)
    'rldscs': (-100, 2000),  # Surface Downwelling Clear-Sky Longwave Radiation (W m-2)
    'rlus': (-100, 2000),  # Surface Upwelling Longwave Radiation (W m-2)
    'rlut': (-100, 2000),  # TOA Outgoing Longwave Radiation (W m-2)
    'rlutcs': (-100, 2000),  # TOA Outgoing Clear-Sky Longwave Radiation (W m-2)
    'rsds': (-100, 3000),  # Surface Downwelling Shortwave Radiation (W m-2)
    'rsdscs': (-100, 3000),  # Surface Downwelling Clear-Sky Shortwave Radiation (W m-2)
    'rsdt': (-100, 3000),  # TOA Incident Shortwave Radiation (W m-2)
    'rsus': (-100, 3000),  # Surface Upwelling Shortwave Radiation (W m-2)
    'rsuscs': (-100, 3000),  # Surface Upwelling Clear-Sky Shortwave Radiation (W m-2)
    'rsut': (-100, 3000),  # TOA Outgoing Shortwave Radiation (W m-2)
    'rsutcs': (-100, 3000),  # TOA Outgoing Clear-Sky Shortwave Radiation (W m-2)
    'rtmt': (-500., 500),  # Net Downward Flux at Top of Model (W m-2)
    'sftlf': (-.01, 100.01),  # Percentage of the grid  cell occupied by land (including lakes) (%)
    'siconc': (0, 101),  # Sea-ice Area Percentage (Ocean Grid) (%)
    'tran': (-.0005, .005),  # Transpiration (kg m-2 s-1)
    'tauu': (-20., 20.),  # Surface Downward Eastward Wind Stress (Pa)
    'tauv': (-20., 20.),  # Surface Downward Northward Wind Stress (Pa)
    'ta': (100, 353),  # Air Temperature (K)
    'tas': (159., 353.),  # Near-Surface Air Temperature (K)
    'tasmax': (164., 386),  # Daily Maximum Near-Surface Air Temperature (K)
    'tasmin': (158., 353.),  # Daily Minimum Near-Surface Air Temperature (K)
    'tos': (-50, 50),  # Sea Surface Temperature (decC)  (!!!)
    'tsl': (159., 353.),  # Temperature of Soil (K)
    'treeFrac': (0, 100.1),  # Tree Cover Percentage (%)
    'zg500': (4000., 6500),  # Geopotential Height at 500hPa (m)
    'zos': (-20., 20), # Sea surface height above_geoid (m)
    'uas': (-50, 50), # Eastward Near-Surface Wind (m s-1)
    'vas': (-50, 50), # Northward Near-Surface Wind (m s-1)
}

# by default the warning range is equivalent to the error range
WARNING_RANGE = ERROR_RANGE.copy()
# update the warning range here
WARNING_RANGE.update({
    'clt': (0., 100.),
    'evspsbl': (0., .0005),
    'hurs': (0., 250.),
    'huss': (0., 1.),
    'mrro': (-1, 1),
    'lai': (0, 20),
    'pr': (0., 0.01),
    'rlds': (-1, 1100),
    'rldscs': (-1, 11000),
    'rlus': (-1, 1100),
    'rlut': (-1, 1100),
    'rlutcs': (-1, 1100),
    'rsds': (-10, 1500),
    'rsdscs': (-10, 1500),
    'rsdt': (-10, 1500),
    'rsus': (-10, 1500),
    'rsuscs': (-10, 1500),
    'rsut': (-10, 1500),
    'rsutcs': (-10, 1500),
    'rtmt': (-300, 300),
    'siconc': (0, 100.01),
    'tas': (178., 333.),
    'tasmin': (178., 333.),
    'tasmax': (178., 333.),
    'tauu': (-1.5, 1.5),
    'tauv': (-1.5, 1.5),
    'tsl': (178., 333.),
    'treeFrac': (0, 100),
    'uas': (-20, 20),
    'vas': (-20, 20),
})
