#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2020-03-05 17:08:23 lukbrunn>

(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import os
import glob
import numpy as np
import xarray as xr

LOAD_PATH = '/cmip6/Next_Generation/sftlf/fx/native'
SAVE_PATH = '/cmip6/Next_Generation/masks/fx/native'

THRESHOLDS = [0, 50, 100]


def sftlf_to_mask(ds, threshold=50):
    ds_mask = ds.copy().rename({'sftlf': 'mask'})
    if threshold == 0:
        ds_mask['mask'].data = np.array(ds['sftlf'].data == 0, dtype=int)
        long_name = f'Mask land fractions of 0%'
        description = ' '.join([
            'Mask with grid cells equal to 1 if they have a land fraction',
            'of 0% and equal to 0 otherwise'])
    elif threshold == 100:
        ds_mask['mask'].data = np.array(ds['sftlf'].data != 100, dtype=int)
        long_name = f'Mask land fractions NOT 100%'
        description = ' '.join([
            'Mask with grid cells equal to 1 if they have a land fraction',
            'of NOT 100% and equal to 0 otherwise'])
    else:
        ds_mask['mask'].data = np.array(ds['sftlf'].data < threshold, dtype=int)
        long_name = f'Mask land fractions below {threshold}%',
        description = ' '.join([
            'Mask with grid cells equal to 1 if they have a land fraction',
            f'below {threshold}% and equal to 0 otherwise'])

    ds_mask['mask'].attrs.update(dict(
        id='mask',
        long_name=long_name,
        description=description,
        standard_name='sea_mask',
        units='1'),
    )

    try:
        ds_mask = ds_mask.drop('file_qf')
    except KeyError:
        pass

    try:
        del ds_mask['seamask'].attrs['comment']
    except KeyError:
        pass

    try:
        del ds_mask['seamask'].attrs['title']
    except KeyError:
        pass

    try:
        del ds_mask['seamask'].attrs['variable_id']
    except KeyError:
        pass

    return ds_mask


def main():
    os.makedirs(SAVE_PATH, exist_ok=True)
    for filename in glob.glob(os.path.join(LOAD_PATH, '*.nc')):
        ds = xr.open_dataset(filename)
        for threshold in THRESHOLDS:
            ds_mask = sftlf_to_mask(ds, threshold)

            save_filename = os.path.basename(filename).replace(
                'sftlf_', f'sftlf{threshold:03d}_')
            ds_mask.to_netcdf(os.path.join(SAVE_PATH, save_filename),
                              format='NETCDF4_CLASSIC')



if __name__ == '__main__':
    main()
