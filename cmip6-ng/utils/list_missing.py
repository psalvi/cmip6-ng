#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2019-03-06 17:29:40 lukbrunn>

(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Convenience script to list all folders in the raw archive with have
not been processed to the next generation archive.

Example
-------
- to find all historical Amon files which are not in <ng-path>/mon/native/ run
> python list_missing.py historical/Amon mon
- to find all Amon files below <ng-path>/mon/g025/
> python list_missing.py '*/Amon' mon -g g025
"""
import os
import glob
import argparse

import string_functions as sf

LOAD_PATH = sf.LOAD_PATH

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument(
    dest='path_rel', type=str,
    help=' '.join([
        f'Path relative to {LOAD_PATH}. List all files below this path which',
        'are not yet in the np-archive. Wildcards (e.g.,"*") may be used.']))
parser.add_argument(
    dest='time_res', type=str, choices=['mon', 'day'],
    help='A valid time resolution folder')
parser.add_argument(
    '--grid_res', '-g', dest='grid_res', type=str, default='native',
    choices=['native', 'g025'], help='A valid grid resolution folder')

args = parser.parse_args()

paths = glob.glob('{}/**/'.format(os.path.join(LOAD_PATH, args.path_rel)), recursive=True)
paths = sorted([path for path in paths if path[-3:-1] in ('gn', 'gr')])

missing = []
for path in paths:
    metadata = sf.split_path(path)
    path_ng = sf.get_ng_path(metadata['varn'], args.time_res)
    path_ng = path_ng.replace('/native/', f'/{args.grid_res}/')

    if not os.path.isdir(path_ng):
        missing.append(path)
        continue

    file_ng = sf.get_ng_filename(
        varn=metadata['varn'],
        time_res=args.time_res,
        model=metadata['model'],
        run_type=metadata['run_type'],
        ensemble=metadata['ensemble'],
    )
    file_ng = file_ng.replace('/native.nc', f'/{args.grid_res}.nc')

    if not os.path.isfile(os.path.join(path_ng, file_ng)):
        missing.append(path)

for miss in missing:
    print(miss)
