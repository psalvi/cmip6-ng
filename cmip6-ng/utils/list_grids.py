#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2020-03-03 16:40:04 lukbrunn>

(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Simple convenience function to extract the grid resolution from all
models.
"""
import os
import glob
import xarray
import numpy as np

PATH = '/net/atmos/data/cmip6/historical/Amon/tas'

print('model          : lat x lon\n')
dlats, dlons = [], []
for model in os.listdir(PATH):
    ensemble = os.listdir(os.path.join(PATH, model))[0]
    if 'gn' in os.listdir(os.path.join(PATH, model, ensemble)):
        grid = 'gn'
    elif 'gr' in os.listdir(os.path.join(PATH, model, ensemble)):
        grid = 'gr'
    elif 'gr1' in os.listdir(os.path.join(PATH, model, ensemble)):
        grid = 'gr1'
    else:
        raise ValueError

    fn = glob.glob('{}/*.nc'.format(os.path.join(PATH, model, ensemble, grid)))[0]
    fullpath = os.path.join(PATH, model, ensemble, grid, fn)

    try:
        ds = xarray.open_dataset(fullpath, decode_cf=False)
        dlat = np.unique(np.around(ds['lat'].data[1:] - ds['lat'].data[:-1], 1))
        dlon = np.unique(np.around(ds['lon'].data[1:] - ds['lon'].data[:-1], 1))

        dlats.append(dlat[0])
        dlons.append(dlon[0])

        if len(dlat) != 1 or len(dlon) != 1:
            raise ValueError

        print(f'{model:<15}: {dlat[0]:.2f} x {dlon[0]:.2f} ({grid})')
    except Exception:
        # breakpoint()
        pass

print(f'\ncoarsest       : {max(dlats):.2f} x {max(dlons):.2f}')
