#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2021-09-06 10:13:01 lukas>

(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Main script to produce the cmip6-ng archive.
"""
import logging
import os
import traceback
from datetime import datetime

from configs.skip_paths import skip_paths
from configs.variable_list import (
    AERDAY_RUN_TYPES,
    AERDAY_VARNS,
    AMON_RUN_TYPES,
    AMON_VARNS,
    AMON_VARNS_PLEV,
    DAY_RUN_TYPES,
    DAY_VARNS,
    EMON_RUN_TYPES,
    EMON_VARNS,
    FX_RUN_TYPES,
    FX_VARNS,
    LMON_RUN_TYPES,
    LMON_VARNS,
    OMON_RUN_TYPES,
    OMON_VARNS,
    SIMON_RUN_TYPES,
    SIMON_VARNS,
)
from core import core_functions as cf
from core import grid_functions as gf
from core import time_functions as tf
from utils import string_functions as sf

CONDA_DEFAULT_ENV = 'iacpy_cmip6_ng'


def main():
    """Call functions"""

    t0 = datetime.now()
    args = cf.read_args()

    format_ = '%(asctime)s - %(levelname)s - %(funcName)s() %(lineno)s: %(message)s'
    logging.basicConfig(
        level=args.log_level,
        filename=args.log_file,
        format=format_)
    logger = logging.getLogger(__name__)

    if os.environ['CONDA_DEFAULT_ENV'] != CONDA_DEFAULT_ENV:
        errmsg = '\n'.join([
            f'conda environment should be "{CONDA_DEFAULT_ENV}"! Please run',
            '  module load conda/2019',
            '  source activate iacpy_cmip6_ng'])
        logger.error(errmsg)
        raise ValueError(errmsg)

    if args.debug:
        git_info = 'skipped (run in debug mode)'
    else:
        git_info = cf.get_git_info()
    logger.info(f'Git: {git_info}')

    # fx
    for files in sf.get_files_all(FX_RUN_TYPES, FX_VARNS, 'fx'):
        filename, updated = cf.process_native_grid(
            files, 'fx',
            git_info=git_info,
            overwrite=args.overwrite,
            skip_paths=skip_paths,
            debug=args.debug)

    # Amon
    for files in sf.get_files_all(AMON_RUN_TYPES, AMON_VARNS, 'Amon'):
        filename = None
        ann_filename = None
        try:
            filename, updated = cf.process_native_grid(
                files, 'mon',
                git_info=git_info,
                overwrite=args.overwrite,
                skip_paths=skip_paths,
                debug=args.debug)
            if filename is not None:  # checks passed -> regrid from native-ng
                gf.regrid_cdo(filename, 'g025', updated, method='con2')
                # calculate annual mean from monthly mean
                ann_filename = tf.yearly_mean_cdo(filename, updated)
                if ann_filename is not None:
                    gf.regrid_cdo(
                        ann_filename, 'g025', updated, method='con2')
        except Exception:
            if ann_filename is not None:  # failed after yearly mean processing
                source = ann_filename
            elif filename is not None:  # failed after native processing
                source = filename
            else:
                source = os.path.dirname(files[0])

            if not gf.delete_corrupt_files(source):  # check/delete corrupt source
                errmsg = '\n'.join([
                    f'Uncovered exception while processing source',
                    f'  {source}',
                    f'  {traceback.format_exc()}'])
                logger.error(errmsg)

    # Amon at given plev in hPa
    plev_index = 0 # counter to cycle through plev slices
    for files in sf.get_files_all(AMON_RUN_TYPES, [var for var,plev in AMON_VARNS_PLEV], 'Amon'):
        filename = None
        ann_filename = None

        AMON_PLEV = AMON_VARNS_PLEV[plev_index][1] # grabbing the plev desired
        try:
            filename, updated = cf.process_native_grid(
                files, 'mon',
                git_info=git_info,
                overwrite=args.overwrite,
                skip_paths=skip_paths,
                debug=args.debug,
                plev_to_extract_hpa=AMON_PLEV) # plev desired passed to native-ng processing function
            if filename is not None:  # checks passed -> regrid from native-ng
                gf.regrid_cdo(filename, 'g025', updated, method='con2')
                # calculate annual mean from monthly mean
                ann_filename = tf.yearly_mean_cdo(filename, updated)
                if ann_filename is not None:
                    gf.regrid_cdo(
                        ann_filename, 'g025', updated, method='con2')
        except Exception:
            if ann_filename is not None:  # failed after yearly mean processing
                source = ann_filename
            elif filename is not None:  # failed after native processing
                source = filename
            else:
                source = os.path.dirname(files[0])

            if not gf.delete_corrupt_files(source):  # check/delete corrupt source
                errmsg = '\n'.join([
                    f'Uncovered exception while processing source',
                    f'  {source}',
                    f'  {traceback.format_exc()}'])
                logger.error(errmsg)
        plev_index += 1

    # Omon
    for files in sf.get_files_all(OMON_RUN_TYPES, OMON_VARNS, 'Omon'):
        filename = None
        try:
            filename, updated = cf.process_native_grid(
                files, 'mon', git_info=git_info, overwrite=args.overwrite, debug=args.debug)
            if filename is not None:  # checks passed -> regrid from native-ng
                gf.regrid_cdo(filename, 'g025', updated, method='dis')
                ann_filename = tf.yearly_mean_cdo(filename, updated)
                if ann_filename is not None:
                    gf.regrid_cdo(ann_filename, 'g025', updated, method='dis')
        except Exception:
            if filename is not None:  # failed after native processing
                source = filename
            else:
                source = os.path.dirname(files[0])

            if not gf.delete_corrupt_files(source):  # check/delete corrupt source
                errmsg = '\n'.join([
                    f'Uncovered exception while processing source',
                    f'  {source}',
                    f'  {traceback.format_exc()}'])
                logger.error(errmsg)

    # Lmon
    for files in sf.get_files_all(LMON_RUN_TYPES, LMON_VARNS, 'Lmon'):
        filename = None
        ann_filename = None
        try:
            filename, updated = cf.process_native_grid(
                files, 'mon', git_info=git_info, overwrite=args.overwrite, debug=args.debug)
            if filename is not None:  # checks passed -> regrid from native-ng
                regrid_method = 'con2'
                if 'mrso_' in filename:
                    regrid_method = 'con'
                gf.regrid_cdo(filename, 'g025', updated, method=regrid_method)
                # calculate annual mean from monthly mean
                ann_filename = tf.yearly_mean_cdo(filename, updated)
                if ann_filename is not None:
                    gf.regrid_cdo(
                        ann_filename, 'g025', updated, method=regrid_method)
        except Exception:
            if ann_filename is not None:  # failed after yearly mean processing
                source = ann_filename
            elif filename is not None:  # failed after native processing
                source = filename
            else:
                source = os.path.dirname(files[0])

            if not gf.delete_corrupt_files(source):  # check/delete corrupt source
                errmsg = '\n'.join([
                    f'Uncovered exception while processing source',
                    f'  {source}',
                    f'  {traceback.format_exc()}'])
                logger.error(errmsg)

    # Emon
    for files in sf.get_files_all(EMON_RUN_TYPES, EMON_VARNS, 'Emon'):
        filename = None
        ann_filename = None
        try:
            filename, updated = cf.process_native_grid(
                files, 'mon', git_info=git_info, overwrite=args.overwrite, debug=args.debug)
            if filename is not None:  # checks passed -> regrid from native-ng
                gf.regrid_cdo(filename, 'g025', updated, method='con2')
                # calculate annual mean from monthly mean
                ann_filename = tf.yearly_mean_cdo(filename, updated)
                if ann_filename is not None:
                    gf.regrid_cdo(
                        ann_filename, 'g025', updated, method='con2')
        except Exception:
            if ann_filename is not None:  # failed after yearly mean processing
                source = ann_filename
            elif filename is not None:  # failed after native processing
                source = filename
            else:
                source = os.path.dirname(files[0])

            if not gf.delete_corrupt_files(source):  # check/delete corrupt source
                errmsg = '\n'.join([
                    f'Uncovered exception while processing source',
                    f'  {source}',
                    f'  {traceback.format_exc()}'])
                logger.error(errmsg)

    # SImon
    for files in sf.get_files_all(SIMON_RUN_TYPES, SIMON_VARNS, 'SImon'):
        filename = None
        try:
            filename, updated = cf.process_native_grid(
                files, 'mon', git_info=git_info, overwrite=args.overwrite, debug=args.debug)
            if filename is not None:  # checks passed -> regrid from native-ng
                gf.regrid_cdo(filename, 'g025', updated, method='dis')
                ann_filename = tf.yearly_mean_cdo(filename, updated)
                if ann_filename is not None:
                    gf.regrid_cdo(ann_filename, 'g025', updated, method='dis')
        except Exception:
            if filename is not None:  # failed after native processing
                source = filename
            else:
                source = os.path.dirname(files[0])

            if not gf.delete_corrupt_files(source):  # check/delete corrupt source
                errmsg = '\n'.join([
                    f'Uncovered exception while processing source',
                    f'  {source}',
                    f'  {traceback.format_exc()}'])
                logger.error(errmsg)

    # day
    for files in sf.get_files_all(DAY_RUN_TYPES, DAY_VARNS, 'day'):
        filename = None
        try:
            filename, updated = cf.process_native_grid(
                files, 'day', git_info=git_info, overwrite=args.overwrite, debug=args.debug)
            if filename is not None:  # checks passed -> regrid from native-ng
                gf.regrid_cdo(filename, 'g025', args.overwrite, method='con2')
        except Exception:
            if filename is not None:  # failed after native processing
                source = filename
            else:
                source = os.path.dirname(files[0])

            if not gf.delete_corrupt_files(source):  # check/delete corrupt source
                errmsg = '\n'.join([
                    f'Uncovered exception while processing source',
                    f'  {source}',
                    f'  {traceback.format_exc()}'])
                logger.error(errmsg)

    # AERday
    for files in sf.get_files_all(AERDAY_RUN_TYPES, AERDAY_VARNS, 'AERday'):
        filename = None
        try:
            filename, updated = cf.process_native_grid(
                files, 'day', git_info=git_info, overwrite=args.overwrite, debug=args.debug)
            if filename is not None:  # checks passed -> regrid from native-ng
                gf.regrid_cdo(filename, 'g025', updated, method='con2')
        except Exception:
            if filename is not None:  # failed after native processing
                source = filename
            else:
                source = os.path.dirname(files[0])

            if not gf.delete_corrupt_files(source):  # check/delete corrupt source
                errmsg = '\n'.join([
                    f'Uncovered exception while processing source',
                    f'  {source}',
                    f'  {traceback.format_exc()}'])
                logger.error(errmsg)

    dt = datetime.now() - t0
    logger.info(f'Success! Total duration: {dt}')


if __name__ == '__main__':
    main()
