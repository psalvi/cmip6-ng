#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2021-04-05 17:02:53 lukas>

(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import logging
import os

import cdo

logger = logging.getLogger(__name__)

CDO = cdo.Cdo()
METHOD_STR = {
    'bil': 'Bilenear interpolation',
    'con2': 'Second order conservative remapping',
    'dis': 'Distance-weighted average remapping',
}


def delete_corrupt_files(filename):
    if not os.path.isfile(filename):
        return False
    try:
        CDO.info(filename)
    except cdo.CDOException:
        logmsg = '\n'.join([
            'Deleting corrupt file:',
            f'  {filename}',
            '  Re-run script to create it again'])
        logger.warning(logmsg)
        os.remove(filename)
        return True
    return False


def _regrid_cdo(load_filename, save_filename, target_grid, method):
    if method == 'bil':
        CDO.remapbil(
            f'../grids/{target_grid}.txt',
            options='-b F64',
            input=load_filename,
            output=save_filename)
    elif method == 'con2':
        CDO.remapcon2(
            f'../grids/{target_grid}.txt',
            options='-b F64',
            input=load_filename,
            output=save_filename)
    elif method == 'con':
        CDO.remapcon(
            f'../grids/{target_grid}.txt',
            options='-b F64',
            input=load_filename,
            output=save_filename)
    elif method == 'dis':
        CDO.remapdis(
            f'../grids/{target_grid}.txt',
            options='-b F64',
            input=load_filename,
            output=save_filename)
    else:
        raise NotImplementedError


def _regrid_ICON_cdo(load_filename, save_filename, target_grid, method):
    # cdo gendis,grid-description-text-file -setgrid,ICON_grid_file one_data_file weightfile
    # two different grid files for ocean and atmosphere, ocean variables
    # are regridded using method 'dis':
    if method == 'dis':
        if not os.path.isfile('../grids/ICON_weightsfile_O.nc'):
            ICON_grid_file = '../grids/icon_grid_0035_modsills_R02B06_O.nc'
            CDO.gendis(f'../grids/{target_grid}.txt',
                       input=f'-setgrid,{ICON_grid_file} {load_filename}',
                       output='../grids/ICON_weightsfile_O.nc')
        CDO.remap(f'../grids/{target_grid}.txt,../grids/ICON_weightsfile_O.nc',
                  options='-b F64',
                  input=load_filename,
                  output=save_filename)
    else:
        if not os.path.isfile('../grids/ICON_weightsfile_A.nc'):
            ICON_grid_file = '../grids/icon_grid_0013_R02B04_G.nc'
            CDO.gendis(f'../grids/{target_grid}.txt',
                       input=f'-setgrid,{ICON_grid_file} {load_filename}',
                       output='../grids/ICON_weightsfile_A.nc')
        CDO.remap(f'../grids/{target_grid}.txt,../grids/ICON_weightsfile_A.nc',
                  options='-b F64',
                  input=load_filename,
                  output=save_filename)


def regrid_cdo(filename, target_grid, overwrite=False, method='bil', cdo_version='1.9.6'):
    if isinstance(cdo_version, str):
        cdo_version = [cdo_version]
    if cdo.getCdoVersion('cdo') not in cdo_version:
        errmsg = f'cdo: {cdo.getCdoVersion("cdo")} not in {cdo_version}'
        logger.error(errmsg)
        raise ValueError(errmsg)

    save_path = os.path.dirname(filename).replace('/native', f'/{target_grid}')
    os.makedirs(save_path, exist_ok=True)
    save_filename = os.path.basename(filename).replace(
        '_native', f'_{target_grid}')
    save_filename = os.path.join(save_path, save_filename)

    if os.path.isfile(save_filename) and not overwrite:
        logger.debug(f'Skip existing file:\n  {save_filename}')
        return None
    elif os.path.isfile(save_filename) and overwrite:
        os.remove(save_filename)  # delete old file to avoid permission errors
    else:
        NotImplementedError

    logger.info(f'Process target file:\n  {save_filename}')
    try:
        if 'ICON-ESM-LR' in filename:
            _regrid_ICON_cdo(filename, save_filename, target_grid, method)
        else:
            _regrid_cdo(filename, save_filename, target_grid, method)
    except cdo.CDOException:
        if os.path.isfile(save_filename):
            os.remove(save_filename)  # make sure no broken file is left
        logger.error(f'cdo error! Regridding failed for source:\n  {filename}')
        delete_corrupt_files(filename)
        return None

    # subprocess.run([
    #     '/usr/local/bin/ncatted', '-a',
    #     f'cmip6-ng,global,a,c,"\ninterpolation_grid = {METHOD_STR[method]} (see CDO)"',
    #     save_filename])

    # logger.info(f'Saved file:\n  {save_filename}')


# def regrid_xesmf(ds, filename, target_grid, overwrite=False, method='bilinear'):
#     save_path = os.path.dirname(filename).replace('/native', f'/{target_grid}')
#     os.makedirs(save_path, exist_ok=True)
#     save_filename = os.path.basename(filename).replace(
#         '_native', f'_{target_grid}__xESMF__{method}__')
#     save_filename = os.path.join(save_path, save_filename)

#     if os.path.isfile(save_filename) and overwrite != 'all':
#         logger.info(f'Skip existing file: {save_filename}')
#         return None
#     else:
#         logger.info(f'Process target file: {save_filename}')

#     if target_grid == 'g025':
#         grid = xarray.Dataset({
#             'lat': ('lat', np.arange(-88.75, 90., 2.5)),
#             'lon': ('lon', np.arange(-178.75, 180., 2.5))})
#     else:
#         NotImplementedError

#     logger.debug(f'Regrid data to {target_grid} using xesmf {method}...')
#     regridder = xesmf.Regridder(ds, grid, method=method, periodic=True, reuse_weights=True)
#     ds_g = xarray.Dataset()
#     for varn in list(ds):
#         if 'lat' in ds[varn].dims and 'lon' in ds[varn].dims:
#             try:
#                 da = regridder(ds[varn])
#             except Exception:
#                 breakpoint()
#             da.attrs = ds[varn].attrs
#             ds_g[varn] = da
#         elif 'lat' not in ds[varn].dims and 'lon' not in ds[varn].dims:
#             ds_g[varn] = ds[varn]
#     attrs = ds.attrs
#     attrs['cmip6-ng'] += f'\nregrid_method = {method}'
#     ds_g.attrs = attrs
#     ds_g.to_netcdf(save_filename, format='NETCDF4_CLASSIC')
#     logger.debug(f'Regrid data to {target_grid} using xesmg {method}... DONE')
