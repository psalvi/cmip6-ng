#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2021-03-15 10:25:45 lukas>

(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract
--------
To fix an issue here please follow the following workflow:
1) report the issue on the IAC wiki:
   https://wiki.iac.ethz.ch/bin/viewauth/Climphys/CMIP6KnownIssues
2) write a failing check for the issue in checks/variable_checks.py or
   checks/basic_checks.py
3) fix the issue here and reference the fix to the wiki
3b) consider appending a (very short) string to applied_fixes (will be added to
    the netCDF global attributes)
3c) consider changing the flag value (0: no changes; 10: minor changes;
    20: major changes)
4) mark the issue as fixed on the wiki


General issues
--------------
#g1) Not all models have latitude and longitude bounds. To be as consistent as
possible we delete we delete them in the models which have them.

#g2) The time units (and values accordingly) get standardised by xarray.
However, this is not true for the time bounds, leading to diffing units and
values between time and time bounds. This is very confusing. Delete bounds for
now.
"""
import logging
from datetime import timedelta

import numpy as np
import xarray as xr

logger = logging.getLogger(__name__)


def apply_fixes(ds, metadata, time_res):
    """
    Apply (model, variable depended) fixes.

    Parameters
    ----------
    ds : xarray.Dataset
    metadata : dictionary

    Returns
    -------
    ds : same as input
    open_issues : list
        List of strings describing not fixable issues
    applies_fixes : string
        String of a semicolon-separated short description of the changes
    flag : int
    """
    model = metadata['model']
    run_type = metadata['run_type']
    varn = metadata['varn']
    open_issues = []
    applied_fixes = []
    flag = 0

    # --- priority fixes ---

    # --- fix issue #30 ---
    if model in ['MCM-UA-1-0']:
        if 'latitude' in ds.dims and 'longitude' in ds.dims:
            ds = ds.rename({'latitude': 'lat', 'longitude': 'lon'})
            flag = 1
    # --- fix issue #30 ---

    # --- universal fixes ---

    # delete latitude bounds (#g1)
    if 'lat' in ds.dims and 'bounds' in ds['lat'].attrs:
        bounds_name = ds['lat'].attrs['bounds']
        del ds[bounds_name]
        del ds['lat'].attrs['bounds']
        applied_fixes.append('delete latitude bounds')

    # delete longitude bounds
    if 'lon' in ds.dims and 'bounds' in ds['lon'].attrs:
        bounds_name = ds['lon'].attrs['bounds']
        del ds[bounds_name]
        del ds['lon'].attrs['bounds']
        applied_fixes.append('delete longitude bounds')

    # delete x bounds
    if 'x' in ds.dims and 'bounds' in ds['x'].attrs:
        bounds_name = ds['x'].attrs['bounds']
        del ds[bounds_name]
        del ds['x'].attrs['bounds']
        applied_fixes.append('delete x bounds')

    # delete y bounds
    if 'y' in ds.dims and 'bounds' in ds['y'].attrs:
        bounds_name = ds['y'].attrs['bounds']
        del ds[bounds_name]
        del ds['y'].attrs['bounds']
        applied_fixes.append('delete longitude bounds')

    # delete time bounds (#g2)
    if 'time' in ds.dims and 'bounds' in ds['time'].attrs:
        bounds_name = ds['time'].attrs['bounds']
        del ds[bounds_name]
        del ds['time'].attrs['bounds']
        applied_fixes.append('delete time bounds')

    # --- fix issue #6 ---
    if model in ['CESM2-WACCM', 'CESM2', 'CESM2-WACCM-FV2', 'CESM2-FV2']:
        if time_res == 'day':
            fix = 'shift time by 12 hours, slice into requested periods'
            ds = ds.assign_coords(time=ds.time - timedelta(seconds=43200))
            if run_type == 'historical':
                ds = ds.sel(time=slice('1850-01-01', '2014-12-31'))
            if run_type in ['ssp119', 'ssp126', 'ssp245', 'ssp370',
                            'ssp434', 'ssp460', 'ssp585', 'ssp585-bgc']:
                ds = ds.sel(time=slice('2015-01-01', '2100-12-31'))
            if run_type in ['ssp534-over','ssp534-over-bgc']:
                ds = ds.sel(time=slice('2040-01-01', '2100-12-31'))
            logger.debug('CESM daily data, ssp time shifted and sliced')
            applied_fixes.append(fix)
            flag = 1
    # --- fix issue #6 ---

    # cut time after 2100
    if run_type in ['ssp119', 'ssp126', 'ssp245', 'ssp370',
                    'ssp434', 'ssp460', 'ssp585',
                    'ssp585-bgc', 'ssp534-over','ssp534-over-bgc']:
        if 'time' in ds.dims and ds.time.dt.year[-1] > 2100:
            ds = ds.sel(time=slice(None, '2100'))
            applied_fixes.append('delete time steps after 2100')
            flag = 1

    # --- model dependent ---

    if model == 'NorCPM1' and time_res != 'fx' and run_type == 'historical':
        ds = ds.sel(time=slice(None, '2014'))
        applied_fixes.append('delete time steps after 2014')
        flag = 1


    if model in ['CESM2-WACCM', 'CESM2']:
        if time_res == 'mon' and run_type == 'land-hist':
            fix = 'remove last year (original file run until 2015)'
            ds = ds.sel(time=slice('1850-01-01', '2014-12-31'))
            applied_fixes.append(fix)
            flag = 1

    if model in ['CESM2']:
        if time_res == 'mon' and run_type == 'amip-piForcing':
            fix = 'remove last year (original file run until 2015)'
            ds = ds.sel(time=slice('1870-01-01', '2014-12-31'))
            applied_fixes.append(fix)
            flag = 1

    if model in ['TaiESM1']:
        if time_res == 'mon' and run_type == 'amip-piForcing':
            fix = 'remove data from 1850-1869'
            ds = ds.sel(time=slice('1870-01-01', '2014-12-31'))
            applied_fixes.append(fix)
            flag = 1

    if model in ['BCC-ESM1']:
        if run_type == 'historical':
            if metadata['varn'] in ['tas', 'tasmin', 'tasmax']:

                # --- flag issue #8 ---
                open_issues.append(' '.join([
                    'first time step does not have tasmin',
                    '<= tas <= tasmax for about 7% of values']))
                flag = 100
                # --- flag issue #8 ---

    # NOTE: see comment in check_tos!
    # if metadata['varn'] in ['siconc', 'tos']:
    #     if model in ['CanESM5', 'UKESM1-0-LL']:

    #         # --- fix issue #13 ---
    #         fix = 'rename (j, i) dimensions to (y, x)'
    #         ds = ds.rename({'j': 'y', 'i': 'x'})
    #         applied_fixes.append(fix)
    #         flag = 1
    #         # --- \fix issue #13\ ---

    # --- fix issue #22 ---
    if model in ['CAMS-CSM1-0']:
        if 'ssp' in run_type and time_res != 'fx' and ds.time.dt.year[-1] == 2099:
            temp = ds.sel(time=slice('2099', '2099'))
            temp[varn].data *= np.nan
            temp['time'].data += timedelta(days=365)
            ds = xr.concat([ds, temp], dim='time')
            flag = 1
            applied_fixes.append('added year 2100 (filled with nan)')
    # --- fix issue #22 ---

    # NOTE: has been fixed upstream
    # # --- fix issue #41 ---
    # if model in ['AWI-CM-1-1-MR']:
    #     if run_type == 'historical' and time_res != 'fx' and ds.time.dt.year[0] == 1851:
    #         temp = ds.sel(time=slice('1851', '1851'))
    #         temp[varn].data *= np.nan
    #         temp['time'].data -= timedelta(days=365)
    #         ds = xr.concat([temp, ds], dim='time')
    #         flag = 1
    #         applied_fixes.append('added year 1850 (filled with nan)')
    # # --- \fix issue #41 ---

    # --- fix issue 31 ---
    if model in ['FGOALS-g3'] and time_res != 'fx' and run_type == 'historical':
        ds = ds.sel(time=slice('1850', '2014'))
        applied_fixes.append('delete time steps after 2014')
        flag = 1
    # --- fix issue 31 ---

    # --- fix issue 23 ---
    if model in ['EC-Earth3'] and time_res != 'fx' and run_type == 'historical':
        ds = ds.sel(time=slice('1850', '2014'))
        applied_fixes.append('delete time steps before 1850')
        flag = 1
    # --- fix issue 23 ---

    # --- fix issue 51 ---
    if model in ['EC-Earth3-Veg'] and time_res == 'day' and run_type == 'ssp245':
        if 'time' in ds.dims and ds.time.dt.year[0] < 2015:
            ds = ds.sel(time=slice('2015-01-01', '2100-12-31'))
            applied_fixes.append('delete time steps before 2015')
            flag = 1
    # --- fix issue 51 ---

    # --- fix issue 49 ---
    if model in ['NorESM2-LM', 'NorESM2-MM']:
        if varn == 'nbp' and 'ssp' in run_type and ds.time.dt.month[0] == 2:
            temp = ds.isel(time=0)
            temp[varn].data *= np.nan
            temp['time'].data -= timedelta(days=31)
            ds = xr.concat([temp, ds], dim='time')
            flag = 1
            applied_fixes.append('added January 2015 (filled with nan)')
    # --- fix issue 49 ---

    # --- fix issue 50 ---
    if model in ['MPI-ESM1-2-LR']:
        if varn in ['nbp', 'mrsos'] and run_type == 'esm-ssp585' and ds.time.dt.year[-1] == 2099:
            temp = ds.sel(time=slice('2099', '2099'))
            temp[varn].data *= np.nan
            temp['time'].data += timedelta(days=365)
            ds = xr.concat([ds, temp], dim='time')
            flag = 1
            applied_fixes.append('added year 2100 (filled with nan)')
    # --- fix issue 50 ---

    # --- fix issue 52 ---
    if model in ['IITM-ESM']:
        if run_type in ['ssp126', 'ssp245', 'ssp370', 'ssp585'] and ds.time.dt.year[-1] == 2099:
            temp = ds.sel(time=slice('2099', '2099'))
            temp[varn].data *= np.nan
            temp['time'].data += timedelta(days=365)
            ds = xr.concat([ds, temp], dim='time')
            flag = 1
            applied_fixes.append('added year 2100 (filled with nan)')
    # --- fix issue 52 ---

    # --- variable dependent ---

    if varn in ['tsl']:
        if 'sdepth' in ds[varn].dims:
            ds = ds.rename({'sdepth': 'depth'})
            applied_fixes.append('change dimension name: sdepth -> depth')
            flag = 1
        elif 'solth' in ds[varn].dims:
            ds = ds.rename({'solth': 'depth'})
            applied_fixes.append('change dimension name: solth -> depth')
            flag = 1


    return ds, open_issues, '; '.join(applied_fixes), flag
