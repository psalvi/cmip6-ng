#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2021-09-22 08:52:41 lukas>

(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

WARNING
-------
Please NEVER EVER do any assignments to the 'ds' object here!!
(The checks functions should never change the dataset in any way)
"""
import logging

import numpy as np

logger = logging.getLogger(__name__)

XY_VARS = [
    'siconc',
    'tos',
    'hfds',
    'zos',
]
Z_VARS = [
    'ta',
]


def check_filename_vs_content(ds, metadata):
    """
    Check all metadata against attributes.

    Parameters
    ----------
    ds : xarray.Dataset
    metadata : dict
    """
    if ds.attrs['experiment_id'] != metadata['run_type']:
        errmsg = ': '.join([
            "ds.attrs['experiment_id'] != metadata['run_type']",
            f"{ds.attrs['experiment_id']} != {metadata['run_type']}"])
    elif ds.attrs['table_id'] != metadata['time_agg']:
        errmsg = ': '.join([
            "ds.attrs['table_id'] != metadata['time_agg']",
            f"{ds.attrs['table_id']} != {metadata['time_agg']}"])
    elif ds.attrs['variable_id'] != metadata['varn']:
        errmsg = ': '.join([
            "ds.attrs['variable_id'] != metadata['varn']",
            f"{ds.attrs['variable_id']} != {metadata['varn']}"])
    elif ds.attrs['source_id'] != metadata['model']:
        errmsg = ': '.join([
            "ds.attrs['source_id'] != metadata['model']",
            f"{ds.attrs['source_id']} != {metadata['model']}"])
    elif ds.attrs['variant_label'] != metadata['ensemble']:
        errmsg = ': '.join([
            "ds.attrs['variant_label'] != metadata['ensemble']",
            f"{ds.attrs['variant_label']} != {metadata['ensemble']}"])
    elif ds.attrs['grid_label'] != metadata['grid']:
        errmsg = ': '.join([
            "ds.attrs['grid_label'] != metadata['grid']",
            f"{ds.attrs['grid_label']} != {metadata['grid']}"])
    else:
        return True  # all test passed

    logger.error(errmsg)
    return False


def check_global_attributes(ds):
    """ """
    return True


def check_time_dimension_all(time, syear, eyear):
    if syear is not None and eyear is not None:
        dyears = eyear - syear + 1
        if time.dt.year[0] != syear or time.dt.year[-1] != eyear:
            errmsg = f'years: {time.dt.year[0].data}-{time.dt.year[-1].data} != {syear}-{eyear}'
            logger.error(errmsg)
            # logger.warning(errmsg)  # NOTE: set this error to debug to avoid spamming
            return False
        if len(np.unique(time.dt.year)) != dyears:
            errmsg = f'#years: {len(np.unique(time.dt.year))} != {dyears}'
            logger.error(errmsg)
            return False

    if time.dt.month[0] != 1 or time.dt.month[-1] != 12:
        errmsg = f'months: {time.dt.month[0].data} != 1 or {time.dt.month[-1].data} != 12'
        # logger.info(errmsg)  # NOTE: decreased level exclude many no-full years in XxCO2 runs
        logger.error(errmsg)
        return False

    return True  # all test passed


def check_time_dimension_mon(time, syear, eyear):
    if syear is not None and eyear is not None:
        dyears = eyear - syear + 1
    else:
        dyears = len(np.unique(time.dt.year))
    if time.size != dyears*12:
        errmsg = f'#months: {time.size} != {dyears*12}'
        logger.error(errmsg)
        return False
    _, counts = np.unique(time.dt.month, return_counts=True)
    if not np.all(counts == dyears):
        errmsg = f'#months: not np.all(counts == dyears)'
        logger.error(errmsg)
        return False

    return True  # all test passed


def check_time_dimension_day(time):
    # extract the calendar
    try:   # might fail for multiple files (xarray.open_mfdataset)
        calendar = time.encoding['calendar']
    except KeyError:
        calendar = time.data[0].calendar

    if calendar == '360_day':
        last_day = 30
    else:
        last_day = 31

    if calendar in ['standard', 'gregorian', 'proleptic_gregorian', 'julian']:
        allowed_days_per_year = [365, 366]
    elif calendar in ['noleap', '365_day']:
        allowed_days_per_year = [365]
    elif calendar in ['all_leap', '366_day']:
        allowed_days_per_year = [366]
    elif calendar in ['360_day']:
        allowed_days_per_year = [360]
    else:
        raise NotImplementedError(f'Calendar {calendar} not known')

    if time.dt.day[0] != 1 or time.dt.day[-1] != last_day:
        errmsg = f'first/last day: {time.dt.day[0]} != 1 or {time.dt.day[-1]} != {last_day}'
        logger.error(errmsg)
        return False
    for (year, tt) in time.groupby('time.year'):
        # TODO: could use 'year' variable to separate leap years

        if tt.size not in allowed_days_per_year:
            errmsg = f'days per year: {tt.size} not in {allowed_days_per_year}'
            logger.error(errmsg)
            return False

        check = False
        for dd in allowed_days_per_year:
            if np.array_equal(tt.dt.dayofyear.data, np.arange(1, dd+1)):
                check = True
        if not check:
            errmsg = f'not all days available for year {year}'
            logger.error(errmsg)
            return False

    return True  # all test passed


def check_time_dimension(ds, metadata, time_res):
    """
    Perform basic time dimension checks.

    Parameters
    ----------
    ds : xarray.Dataset
    metadata : dict
    time_res : string

    Implemented checks
    ------------------
    - start year, end year, number of years
    # - full range of bounds
    - month:
      * start month, end month, number of months
    """
    if time_res == 'fx':
        return True

    if 'time' not in list(ds.dims):
        errmsg = f"Dimension 'time' not found in {list(ds.dims)}"
        logger.error(errmsg)
        return None

    time = ds['time']
    if metadata['run_type'] in ['historical', 'land-hist']:
        pass_ = check_time_dimension_all(time, 1850, 2014)
        if not pass_:
            return False
        if time_res == 'mon':
            pass_ = check_time_dimension_mon(time, 1850, 2014)
        elif time_res == 'day':
            pass_ = check_time_dimension_day(time)
        else:
            raise NotImplementedError
        if not pass_:
            return False
    elif metadata['run_type'] in [
            'ssp119', 'ssp126', 'ssp245', 'ssp370', 'ssp434',
            'ssp460', 'ssp585', 'esm-ssp585']:
        pass_ = check_time_dimension_all(time, 2015, 2100)
        if not pass_:
            return False
        if time_res == 'mon':
            pass_ = check_time_dimension_mon(time, 2015, 2100)
        elif time_res == 'day':
            pass_ = check_time_dimension_day(time)
        else:
            raise NotImplementedError
        if not pass_:
            return False
    elif metadata['run_type'] == 'ssp534-over':
        # 'overshoot of 3.4 W/m**2 branching from ssp585 in 2040'
        pass_ = check_time_dimension_all(time, 2040, 2100)
        if not pass_:
            return False
        if time_res == 'mon':
            pass_ = check_time_dimension_mon(time, 2040, 2100)
        elif time_res == 'day':
            pass_ = check_time_dimension_day(time)
        else:
            raise NotImplementedError
        if not pass_:
            return False
    elif metadata['run_type'] in [
            'piControl', '1pctCO2', 'abrupt-2xCO2', 'abrupt-4xCO2',
            '1pctCO2-bgc', 'esm-1pctCO2', 'esm-hist', 'esm-ssp585',
            'hist-bgc', 'hist-nat', 'hist-aer', 'hist-GHG',
            'ssp534-over-bgc', 'ssp585-bgc', 'hist-1950', 'control-1950']:
        pass_ = check_time_dimension_all(time, None, None)
        if not pass_:
            return False
        if time_res == 'mon':
            pass_ = check_time_dimension_mon(time, None, None)
        elif time_res == 'day':
            pass_ = check_time_dimension_day(time)
        else:
            raise NotImplementedError
        if not pass_:
            return False
    elif metadata['run_type'] == 'amip-piForcing':
        pass_ = check_time_dimension_all(time, 1870, 2014)
        if not pass_:
            return False
        if time_res == 'mon':
            pass_ = check_time_dimension_mon(time, 1870, 2014)
        elif time_res == 'day':
            pass_ = check_time_dimension_day(time)
        else:
            raise NotImplementedError
        if not pass_:
            return False
    else:
        raise NotImplementedError

    return True  # all test passed


def check_time_decoding(ds, time_res, starttime, endtime):
    """
    Special check for the time axis: test if the file contains the
    correct number of time steps based on the filename. Note that
    all monthly files with non-full years do not fulfill this criterion!

    Parameters
    ----------
    ds : xarray.Dataset
    time_res : strong, {'ann', 'mon', 'day'}
    starttime : string
        Must have the from 'YYYYMM'
    endtime : string
        Must have the form 'YYYYMM'

    Return
    ------
    pass_ : bool
    """
    if 'time' not in list(ds.dims) or time_res == 'day':
        return True

    syear = int(starttime[:4])
    eyear = int(endtime[:4])
    nr_years = eyear - syear + 1
    nr_months = nr_years * 12

    if time_res == 'ann':
        if nr_years != ds['time'].size:
            return False
    elif time_res == 'mon':
        if nr_months != ds['time'].size:
            return False
    else:
        raise NotImplementedError
    return True


def check_latitude_dimension(ds, metadata):
    """
    Perform basic latitude dimension checks.

    Parameters
    ----------
    ds : xarray.Dataset

    Implemented checks
    ------------------
    - range
    - strictly increasing

    Returns
    -------
    pass_ : bool
    """
    if metadata['model'] in ['ICON-ESM-LR']:
        # ICON does not have lat lon grid
        return True

    if 'lat' not in list(ds.dims):
        errmsg = f"Dimension 'lat' not found in {list(ds.dims)}"
        logger.error(errmsg)
        return None
    lat = ds['lat']
    if lat.attrs['units'] != 'degrees_north':
        errmsg = f'latitude units: {lat.attrs["units"]} != degrees_north'
        logger.error(errmsg)
        return False
    if lat.min() < -90. or lat.max() > 90.:
        errmsg = f'latitude extent: {lat.min()} < -90 or {lat.max()} > 90'
        logger.error(errmsg)
        return False
    delta_lat = lat.data[1:] - lat.data[:-1]
    if not np.all(delta_lat > 0.):
        errmsg = f'latitude values not increasing'
        logger.error(errmsg)
        return False

    return True  # all test passed


def check_longitude_dimension(ds, metadata):
    """
    Perform basic longitude dimension checks.

    Parameters
    ----------
    ds : xarray.Dataset

    Implemented checks
    ------------------
    - range
    - strictly increasing
    - equidistant

    Returns
    -------
    pass_ : bool
    """
    if metadata['model'] in ['ICON-ESM-LR']:
        # ICON does not have lat lon grid
        return True

    if 'lon' not in list(ds.dims):
        errmsg = f"Dimension 'lon' not found in {list(ds.dims)}"
        logger.error(errmsg)
        return None
    lon = ds['lon']
    if lon.attrs['units'] != 'degrees_east':
        errmsg = f'longitude units: {lon.attrs["units"]} != degrees_east'
        logger.error(errmsg)
        return False
    if lon.min() < 0. or lon.max() > 360.:
        errmsg = f'longitude extent: {lon.min()} < 0 or {lon.max()} > 360'
        logger.error(errmsg)
        return False
    delta_lon = np.around(lon.data[1:] - lon.data[:-1], decimals=3)
    if not np.all(delta_lon > 0.):
        errmsg = f'longitude values not increasing'
        logger.error(errmsg)
        return False
    if len(np.unique(delta_lon)) != 1:
        errmsg = f'longitudes values not equidistant: {np.unique(delta_lon)}'
        logger.error(errmsg)
        return False

    return True  # all test passed


def check_pressure_level(ds):
    """
    Perform basic pressure level dimension chekcs.

    Parameters
    ----------
    ds : xarray.Dataset

    Implemented checks
    ------------------
    - correct name
    - correct unit
    - attribute: positive=down
    - decreasing with altitude (and with index)
    - larger zero

    Returns
    -------
    pass_ : bool
    """
    if 'plev' not in list(ds.dims):
        errmsg = f"Dimension 'plev' not found in {list(ds.dims)}"
        logger.error(errmsg)
        return None
    plev = ds['plev']
    if plev.attrs['units'] != 'Pa':
        errmsg = f'pressure level units: {plev.attrs["units"]} != Pa'
        logger.error(errmsg)
        return False
    if plev.attrs['positive'] != 'down':
        errmsg = f'plev with attribute positive={plev.attrs["positive"]} is not yet implemented!'
        raise NotImplementedError(errmsg)
    if plev.min() < 0.:
        errmsg = 'negative pressure level found!'
        logger.error(errmsg)
        return False
    delta_plev = plev.data[1:] - plev.data[:-1]
    if not np.all(delta_plev < 0.):
        errmsg = f'pressure levels are not decreasing'
        logger.error(errmsg)
        return False

    return True


def basic_checks(ds, metadata, time_res):
    pass_ = check_filename_vs_content(ds, metadata)
    if not pass_:
        return False

    pass_ = check_global_attributes(ds)
    if not pass_:
        return False

    pass_ = check_time_dimension(ds, metadata, time_res)
    if not pass_:
        return False

    if metadata['varn'] in XY_VARS:
        pass
    else:
        pass_ = check_latitude_dimension(ds, metadata)
        if not pass_:
            return False
        pass_ = check_longitude_dimension(ds, metadata)
        if not pass_:
            return False
        if metadata['varn'] in Z_VARS:
            pass_ = check_pressure_level(ds)
            if not pass_:
                return False

    return True  # all test passed
